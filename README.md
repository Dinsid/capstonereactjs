# DỰ ÁN CAPSTONE REACTJS BC35 NHÓM 13

### Thành viên tham gia dự án

#### Mã Việt Hà : Trang đăng nhập, trang homepage

#### Nguyễn Sĩ Dũng : Trang đăng ký, trang detail

##### Link source code : [https://gitlab.com/Dinsid/capstonereactjs](https://gitlab.com/Dinsid/capstonereactjs)

##### Link deploy : [https://capstonereactjs.vercel.app/](https://capstonereactjs.vercel.app/)

##### Link youtube : [https://youtu.be/5gmJYk3p87Q](https://youtu.be/5gmJYk3p87Q)

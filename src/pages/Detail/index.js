import { Breadcrumb, Modal, message } from 'antd';
import React, { useEffect, useState } from 'react';
import ReactPlayer from 'react-player';
import { useParams } from 'react-router-dom';
import { getMovieDetail } from '../../services';
import MovieTab from './detailTab/MovieTab';
import moment from 'moment';
import 'moment/locale/vi';

export default function DetailPage() {
    const [modelDetail, setModelDetail] = useState(false);
    const [detailMovie, setDetailMovie] = useState([]);
    const [messageApi, contextHolder] = message.useMessage();

    let { id } = useParams();

    const error = (messageErr) => {
        messageApi.open({
            type: 'error',
            content: `Lỗi: ${messageErr}`,
        });
    };

    const showModal = () => {
        setModelDetail(true);
    };

    const handleCancel = () => {
        setModelDetail(false);
    };

    useEffect(() => {
        getMovieDetail(id)
            .then((res) => {
                setDetailMovie(res.content);
            })
            .catch((err) => {
                error(err);
            });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [id]);

    const renderMovieDetail = () => {
        return (
            <div className="mt-10 mx-24 ">
                {contextHolder}
                <div className="p-10 bg-[#9fd07f] flex items-center  ">
                    <div className="w-3/6">
                        <img
                            className="  hover:rotate-12 duration-700 rounded-2xl h-72  "
                            src={detailMovie?.hinhAnh}
                            alt=""
                        />
                    </div>
                    <div className=" space-y-5 text-[20px] w-3/4   ">
                        <h2>
                            <span className="font-medium">Tên Phim:{detailMovie?.tenPhim}</span>{' '}
                        </h2>
                        <h2>
                            <span className="font-medium">Khởi Chiếu: </span>
                            {moment(detailMovie?.ngayKhoiChieu).format('LLL')}
                        </h2>
                        <h2>
                            <span className="font-medium">Mô tả:</span> {detailMovie?.moTa}
                        </h2>
                        <h2>
                            <span className="font-medium">Đánh giá:</span> {detailMovie?.danhGia}/10
                        </h2>

                        <div className=" flex   duration-1000 ">
                            <button
                                className="focus:outline-none text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:ring-red-300 font-medium rounded-lg text-sm px-5 py-2.5 mb-3 dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-900 mr-5"
                                onClick={showModal}
                            >
                                Xem Trailer
                            </button>
                            <button
                                type="button"
                                className="focus:outline-none text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:ring-red-300 font-medium rounded-lg text-sm px-5 py-2.5 mb-3 dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-900"
                            >
                                ĐẶT VÉ NGAY
                            </button>

                            <Modal
                                width="710px"
                                height="400px"
                                title="Trailer Phim"
                                open={modelDetail}
                                onCancel={handleCancel}
                            >
                                <ReactPlayer
                                    width="650px"
                                    playing={modelDetail}
                                    height="360px"
                                    url={detailMovie?.trailer}
                                />
                            </Modal>
                        </div>
                    </div>
                </div>
            </div>
        );
    };

    return (
        <div className="pb-5">
            <div className="font-black w-3/4 mx-auto ">
                <Breadcrumb className="text-xl">
                    <Breadcrumb.Item>
                        <span>{detailMovie?.tenPhim}</span>
                    </Breadcrumb.Item>
                </Breadcrumb>
            </div>

            {renderMovieDetail()}
            <h3 className="mx-24 my-10 text-3xl font-semibold border-b-2 z-10 px-5 py-2">Lịch Chiếu Phim</h3>
            <MovieTab />
        </div>
    );
}

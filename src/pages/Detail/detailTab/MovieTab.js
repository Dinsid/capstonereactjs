import { useEffect, useState } from 'react';
import { getMovieByTheater } from './../../../services/index';
import MovieItemTab from './MovieItemTab';
import { Tabs, message } from 'antd';

export default function MovieTab() {
    const [dataMovie, setDataMovie] = useState([]);
    const [messageApi, contextHolder] = message.useMessage();

    const error = (messageErr) => {
        messageApi.open({
            type: 'error',
            content: `Lỗi: ${messageErr}`,
        });
    };

    useEffect(() => {
        getMovieByTheater()
            .then((res) => {
                setDataMovie(res.content);
            })
            .catch((err) => {
                error(err);
            });

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const renderDanhSachPhimTheoCumRap = (cumRap) => {
        return (
            <div>
                {contextHolder}
                {cumRap.danhSachPhim.map((movie, index) => {
                    return <MovieItemTab key={index} movie={movie} />;
                })}
            </div>
        );
    };

    const renderCumRapTheoHeThongRap = (heThongRap) => {
        return heThongRap.lstCumRap.map((cumRap, index) => {
            return {
                label: (
                    <div className="w-44">
                        <h4>{cumRap.tenCumRap}</h4>
                        <p className="truncate">{cumRap.diaChi}</p>
                    </div>
                ),
                key: cumRap.maCumRap,
                children: <div style={{ height: 400, overflow: 'scroll' }}>{renderDanhSachPhimTheoCumRap(cumRap)}</div>,
            };
        });
    };

    const renderHeThongRap = () => {
        return dataMovie.map((heThongRap, index) => {
            return {
                label: <img className="w-16 h-16" src={heThongRap.logo} alt="" />,
                key: heThongRap.maHeThongRap,
                children: (
                    <Tabs
                        style={{
                            height: 400,
                        }}
                        tabPosition="left"
                        defaultActiveKey="1"
                        items={renderCumRapTheoHeThongRap(heThongRap)}
                    />
                ),
            };
        });
    };

    return (
        <div className="container mx-auto mt-20 xx:hidden xl:block">
            <Tabs
                style={{
                    height: 400,
                }}
                tabPosition="left"
                defaultActiveKey="1"
                items={renderHeThongRap()}
            />
        </div>
    );
}

import { message } from 'antd';
import { useEffect, useState } from 'react';
import MovieItem from '../../components/MovieItem';
import NewFeed from '../../components/NewFeed';
import Pagination from '../../components/Pagination';
import { paginationMovie } from '../../services';
import MovieTab from './../Detail/detailTab/MovieTab';

function Home() {
    const [movies, setMovies] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [totalPage, setTotalPage] = useState(0);
    const [code, setCode] = useState('GP01');
    const [messageApi, contextHolder] = message.useMessage();

    const handleSetCurrentPage = (number) => {
        if (number !== currentPage) {
            setCurrentPage(number);
        }
    };

    const error = (message) => {
        messageApi.open({
            type: 'error',
            content: message,
        });
    };

    const handleSearchGroupCode = async (value) => {
        if (value !== code && value !== '') {
            const result = await paginationMovie(currentPage, value);

            if (result.content !== 'Mã nhóm không hợp lệ !') {
                setMovies(result.content.items);
                setTotalPage(result.content.totalPages);
                setCode(value);
            } else {
                error(result.content);
                setCode('GP01');
            }
        } else if (value === code) {
            error('Không nhập trùng mã nhóm hiện tại');
        } else {
            error('Lỗi vui lòng thử lại');
        }
    };

    useEffect(() => {
        const fetchApi = async () => {
            const result = await paginationMovie(currentPage, code);

            if (result.content !== 'Mã nhóm không hợp lệ !') {
                setMovies(result.content.items);
                setTotalPage(result.content.totalPages);
                setCurrentPage(result.content.currentPage);
            } else {
                error('Lỗi lấy danh sách phim');
            }
        };
        fetchApi();

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [currentPage]);

    return (
        <div className="text-center">
            <div className="flex-wrap flex mx-[-12px] md:mx-[-12px] xx:mx-[-4px]">
                {movies.map((movie) => (
                    <MovieItem key={movie.maPhim} movie={movie} />
                ))}
            </div>
            {contextHolder}
            <div className="mx-auto">
                <Pagination
                    totalPage={totalPage}
                    currentPage={currentPage}
                    handleSetCurrentPage={handleSetCurrentPage}
                />
            </div>
            <NewFeed code={code} handleSearchGroupCode={handleSearchGroupCode} />

            <MovieTab />
        </div>
    );
}

export default Home;

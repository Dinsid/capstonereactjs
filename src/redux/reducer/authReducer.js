import { createSlice } from '@reduxjs/toolkit';

const authReducer = createSlice({
    name: 'auth',
    initialState: {
        login: {
            currentUser: null,
        },
    },
    reducers: {
        loginUserSuccess: (state, action) => {
            state.login.currentUser = action.payload;
        },
        logoutUserSuccess: (state) => {
            state.login.currentUser = null;
        },
    },
});

export const { loginUserSuccess, logoutUserSuccess } = authReducer.actions;

export default authReducer.reducer;

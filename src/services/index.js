import * as request from '../utils/index';
import { loginUserSuccess } from '../redux/reducer/authReducer';

export const paginationMovie = async (soTrang, code) => {
    try {
        const response = await request.get(
            `/QuanLyPhim/LayDanhSachPhimPhanTrang?maNhom=${code}&soTrang=${soTrang}&soPhanTuTrenTrang=10`
        );

        return response;
    } catch (error) {
        return error.response.data;
    }
};

export const loginUser = async (user, dispatch) => {
    try {
        const response = await request.post(`/QuanLyNguoiDung/DangNhap`, user);

        dispatch(loginUserSuccess(response.content));
        const { content, ...other } = response;

        return { ...other };
    } catch (error) {
        return error.response.data;
    }
};

export const getMovieByTheater = () => {
    return request.get(`/QuanLyRap/LayThongTinLichChieuHeThongRap`);
};

export const getMovieDetail = (data) => {
    return request.get(`/QuanLyPhim/LayThongTinPhim?MaPhim=${data}`);
};

export const postRegister = (data) => {
    return request.post(`/QuanLyNguoiDung/DangKy`, data);
};

import Home from '../pages/Home';
import Login from '../layouts/components/Login';
import Register from '../layouts/components/Register';
import Detail from '../pages/Detail';

const routerPublic = [
    { path: '/', component: Home },
    { path: '/login', component: Login, layout: null },
    { path: '/register', component: Register, layout: null },
    { path: '/detail/:id', component: Detail },
];

export default routerPublic;

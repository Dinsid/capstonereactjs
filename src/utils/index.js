import axios from 'axios';

const TOKEN =
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCAzNSIsIkhldEhhblN0cmluZyI6IjEwLzA2LzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY4NjM1NTIwMDAwMCIsIm5iZiI6MTY1NzczMTYwMCwiZXhwIjoxNjg2NTAyODAwfQ.RfqXFAkX0NK9-sSoSak2_Ys49ENugB0G2-zkJO_cEjQ';

const request = axios.create({
    baseURL: 'https://movienew.cybersoft.edu.vn/api',
    headers: {
        TokenCybersoft: TOKEN,
    },
});

export const get = async (path, option = {}) => {
    const res = await request.get(path, option);

    return res.data;
};

export const post = async (path, option = {}) => {
    const res = await request.post(path, option);

    return res.data;
};

export default request;

import { Fragment } from 'react';
import Header from '../components/Header';

function DefaultLayout({ children }) {
    return (
        <Fragment>
            <Header />
            <div className="container mt-20 h-screen mx-auto">{children}</div>
        </Fragment>
    );
}

export default DefaultLayout;

import { Button, Form, Input } from 'antd';
import { useDispatch } from 'react-redux';
import { Link, useNavigate } from 'react-router-dom';
import { loginUser } from '../../../services';
import LayoutForm from '../../../components/LayoutForm';

function Login() {
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const onFinish = async (values) => {
        const result = await loginUser(values, dispatch);

        if (result.statusCode === 200) {
            navigate('/');
        } else {
            alert(`Error: ${result.message}`);
        }
    };

    return (
        <LayoutForm>
            <h2 className="font-bold text-3xl uppercase text-stone-500 mt-20 mb-28 relative xx:mb-10 xx:mt-0 xx:text-2xl md:text-3xl">
                Đăng nhập vào Cinema
            </h2>
            <Form
                name="basic"
                labelCol={{
                    span: 4,
                }}
                wrapperCol={{
                    span: 20,
                }}
                initialValues={{
                    remember: true,
                }}
                onFinish={onFinish}
                autoComplete="off"
            >
                <Form.Item
                    label="Tài khoản"
                    name="taiKhoan"
                    rules={[
                        {
                            required: true,
                            message: 'Vui lòng nhập tài khoản!',
                        },
                    ]}
                >
                    <Input placeholder="Nhập tài khoản của bạn" />
                </Form.Item>

                <Form.Item
                    label="Mật khẩu"
                    name="matKhau"
                    rules={[
                        {
                            required: true,
                            message: 'Vui lòng nhập mật khẩu!',
                        },
                    ]}
                >
                    <Input.Password placeholder="Nhập mật khẩu của bạn" />
                </Form.Item>

                <Form.Item
                    wrapperCol={{
                        offset: 0,
                        span: 24,
                    }}
                >
                    <Button type="primary" htmlType="submit" className="bg-blue-600 text-left">
                        Đăng nhập
                    </Button>
                </Form.Item>
            </Form>
            <span className="text-sm xx:text-xs md:text-lg xl:text-sm">
                Bạn chưa có tài khoản?
                <Link to="/register" className="text-blue-500 font-semibold hover:text-blue-700 ml-1">
                    Đăng ký ngay
                </Link>
            </span>
        </LayoutForm>
    );
}

export default Login;

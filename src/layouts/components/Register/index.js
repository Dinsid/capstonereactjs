import { Button, Form, Input, message } from 'antd';
import { Link } from 'react-router-dom';
import LayoutForm from '../../../components/LayoutForm';
import { postRegister } from './../../../services/index';
import { SET_USER_REGISTER } from './../../../redux/constant/userConstant';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';

function Register() {
    let navigate = useNavigate();
    let dispatch = useDispatch();
    const onFinish = async (values) => {
        postRegister(values)
        .then((res) => {
            message.success("Đăng kí thành công");
            dispatch({type: SET_USER_REGISTER, payload: res.data.content});
            setTimeout(() => {
                navigate("/login");
              }, 1000);
              })
              .catch((err) => {
                message.error("Đăng kí thất bại");

              });
    };

    return (
        <LayoutForm>
            <h2 className="font-bold text-3xl uppercase text-stone-500 mt-20 mb-28 relative xx:mt-0 xx:mb-8">
                Đăng ký tài khoản Cinema
            </h2>
            <Form
                name="basic"
                labelCol={{
                    span: 4,
                }}
                wrapperCol={{
                    span: 20,
                }}
                initialValues={{
                    remember: true,
                }}
                onFinish={onFinish}

                autoComplete="off"
            >
                <Form.Item
                    label="Tài khoản"
                    name="taiKhoan"
                    rules={[
                        {
                            required: true,
                            message: 'Vui lòng nhập tài khoản!',
                        },
                    ]}
                >
                    <Input placeholder="Nhập tài khoản của bạn" />
                </Form.Item>

                <Form.Item
                    label="Mật khẩu"
                    name="matKhau"
                    rules={[
                        {
                            required: true,
                            message: 'Vui lòng nhập mật khẩu!',
                        },
                    ]}
                >
                    <Input.Password placeholder="Nhập mật khẩu của bạn" />
                </Form.Item>

                <Form.Item
                    label="Email"
                    name="email"
                    rules={[
                        {
                            required: true,
                            message: 'Vui lòng nhập email!',
                        },
                    ]}
                >
                    <Input placeholder="Nhập email của bạn" />
                </Form.Item>

                <Form.Item
                    label="Số ĐT"
                    name="soDt"
                    rules={[
                        {
                            required: true,
                            message: 'Vui lòng nhập số điện thoại!',
                        },
                    ]}
                >
                    <Input placeholder="Nhập số điện thoại của bạn" />
                </Form.Item>

                <Form.Item
                    label="Mã nhóm"
                    name="maNhom"
                    rules={[
                        {
                            required: true,
                            message: 'Vui lòng nhập mã nhóm!',
                        },
                    ]}
                >
                    <Input placeholder="Nhập mã nhóm của bạn" />
                </Form.Item>

                <Form.Item
                    label="Họ tên"
                    name="hoTen"
                    rules={[
                        {
                            required: true,
                            message: 'Vui lòng nhập họ tên!',
                        },
                    ]}
                >
                    <Input placeholder="Nhập họ tên của bạn" />
                </Form.Item>

                <Form.Item
                    wrapperCol={{
                        offset: 0,
                        span: 24,
                    }}
                >
                    <Button type="primary" htmlType="submit" className="bg-blue-600 text-left">
                        Đăng ký
                    </Button>
                </Form.Item>
                
            </Form>
            
            <span className="text-sm">
                Bạn đã có tài khoản?
                <Link to="/login" className="text-blue-500 font-semibold hover:text-blue-700 ml-1">
                    Đăng nhập ngay
                </Link>
            </span>
        </LayoutForm>
    );
}

export default Register;

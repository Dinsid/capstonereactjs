import { Dropdown } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useNavigate } from 'react-router-dom';
import { Image } from '../../../assets/image';
import { logoutUserSuccess } from '../../../redux/reducer/authReducer';

function Header() {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const currentUser = useSelector((state) => state.auth.login.currentUser);

    const handleLogoutUser = () => {
        if (currentUser) {
            dispatch(logoutUserSuccess());
            navigate('/login');
        }
    };

    const items = [
        {
            label: (
                <button onClick={handleLogoutUser} className="text-lg xl:text-sm xl:p-0 px-2 py-1">
                    Đăng xuất
                </button>
            ),
        },
    ];

    return (
        <div className="h-20 fixed top-[-1px] left-0 right-0 shadow-lg z-10 bg-white flex items-center justify-between px-20 xx:px-8">
            <Link to="/" className="">
                <span className="text-orange-700 text-3xl font-medium">Cinema</span>
            </Link>

            {currentUser ? (
                <div className="flex items-center">
                    <span className="mr-1 xx:hidden xl:block">Hi! </span>
                    <span className="mr-3 font-bold xx:hidden xl:block">{currentUser.hoTen}</span>
                    <Dropdown
                        menu={{
                            items,
                        }}
                        trigger={['click']}
                    >
                        <img
                            src={Image.avatar}
                            alt={currentUser.hoTen}
                            className="w-10 h-10 rounded-full object-cover cursor-pointer"
                        />
                    </Dropdown>
                </div>
            ) : (
                <Link to="/login" className="px-5 py-2 bg-slate-800 rounded text-white">
                    Đăng nhập
                </Link>
            )}
        </div>
    );
}

export default Header;

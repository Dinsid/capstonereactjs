function Pagination({ totalPage, currentPage, handleSetCurrentPage }) {
    const pages = [];
    for (let i = 1; i <= totalPage; i++) {
        pages.push(
            <button
                key={i}
                className={
                    currentPage === i
                        ? 'px-4 py-1 border-2 border-sky-500 mx-2 border-solid rounded hover:bg-slate-100'
                        : 'px-4 py-1 hover:bg-slate-200 rounded border-2'
                }
                onClick={() => handleSetCurrentPage(i)}
            >
                {i}
            </button>
        );
    }

    return (
        <div className="mt-4">
            <button
                className={
                    currentPage === 1
                        ? 'px-4 py-1 hover:cursor-not-allowed text-neutral-600'
                        : 'px-4 py-1 hover:bg-slate-200 rounded font-semibold'
                }
                onClick={currentPage === 1 ? null : () => handleSetCurrentPage(currentPage - 1)}
            >
                <i className="fa-solid fa-angle-left"></i>
            </button>

            {pages.map((page) => page)}

            <button
                className={
                    currentPage === totalPage
                        ? 'px-4 py-1 hover:cursor-not-allowed text-neutral-600'
                        : 'px-4 py-1 hover:bg-slate-200 rounded font-semibold'
                }
                onClick={currentPage === totalPage ? null : () => handleSetCurrentPage(currentPage + 1)}
            >
                <i className="fa-solid fa-angle-right"></i>
            </button>
        </div>
    );
}

export default Pagination;

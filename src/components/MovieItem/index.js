import { Card } from 'antd';
import { Link } from 'react-router-dom';

function MovieItem({ movie }) {
    return (
        <div className="w-1/5 xl:w-1/4 md:w-1/2 my-5 px-[12px] md:px-[20px] xs:w-1/2 xx:w-full xx:px-[12px]">
            <Card
                className="mx-auto h-[390px] md:h-[350px] xs:h-[280px] xx:h-[320px]"
                hoverable
                style={{
                    width: '100%',
                    position: 'relative',
                    cursor: 'text',
                }}
                cover={
                    <img
                        className="max-h-64 md:h-[240px] xx:h-[220px] xs:h-[180px] object-cover"
                        alt={movie.tenPhim}
                        src={movie.hinhAnh}
                    />
                }
            >
                <h3
                    className="font-bold mb-4 text-ellipsis overflow-hidden whitespace-nowrap md:text-lg uppercase"
                    title={movie.tenPhim}
                >
                    {movie.tenPhim}
                </h3>
                <Link to={`/detail/${movie.maPhim}`} className="py-2 px-3 rounded bg-slate-200 md:text-lg xl:text-base">
                    Xem chi tiết
                </Link>
            </Card>
        </div>
    );
}

export default MovieItem;

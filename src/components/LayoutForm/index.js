import Lottie from 'lottie-react';
import coding from '../../svg/coding-slide.json';

function LayoutForm({ children }) {
    return (
        <div className="h-screen bg-stone-200 flex justify-center items-center">
            <div className="bg-white p-5 flex max-w-[1578px] xl:h-[80vh] xx:h-full xl:items-center md:w-full">
                <div className="w-1/2 xx:hidden xl:block">
                    <Lottie animationData={coding} />
                </div>

                <div className="w-1/2 text-center p-10 xx:w-full md:mt-20 xl:mt-0">{children}</div>
            </div>
        </div>
    );
}

export default LayoutForm;

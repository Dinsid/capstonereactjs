import { Input, Space } from 'antd';
import { useState } from 'react';
const { Search } = Input;

function NewFeed({ code, handleSearchGroupCode }) {
    const [active, setActive] = useState(false);

    const onSearch = (value) => {
        handleSearchGroupCode(value);
        setActive(false);
    };

    return (
        <div className="fixed left-16 bottom-28 rounded-full bg-slate-400 w-12 h-12 cursor-pointer flex items-center justify-center xx:hidden xl:block">
            <div className="relative xl:top-[10px]">
                <span className="font-bold text-xs px-2 py-3 rounded-full" onClick={() => setActive(!active)}>
                    {code}
                </span>
                {active && (
                    <div className="absolute top-[-66px] bg-white py-2 px-4 z-20 shadow-lg rounded">
                        <Space direction="vertical">
                            <Search
                                placeholder="Nhập mã nhóm phim"
                                allowClear
                                onSearch={onSearch}
                                style={{
                                    width: 240,
                                }}
                            />
                        </Space>
                    </div>
                )}
            </div>
        </div>
    );
}

export default NewFeed;
